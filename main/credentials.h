/*

Credentials file

*/

#pragma once

// Only one of these settings must be defined
//#define USE_ABP
#define USE_OTAA

#ifdef USE_ABP

    // LoRaWAN NwkSKey, network session key MSB format
    static const u1_t PROGMEM NWKSKEY[16] = {  };
    // LoRaWAN AppSKey, application session key MSB format
    static const u1_t PROGMEM APPSKEY[16] = {  };
    // LoRaWAN end-device address (DevAddr)
    // This has to be unique for every node
    static const u4_t DEVADDR = 0x260110C6;

#endif

#ifdef USE_OTAA

    // This EUI must be in little-endian format, so least-significant-byte
    // first. When copying an EUI from ttnctl output, this means to reverse
    // the bytes. For TTN issued EUIs the last bytes should be 0x00, 0x00,
    // 0x00.
    static const u1_t PROGMEM APPEUI[8]  = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    // This should also be in little endian format, see above.
    static const u1_t PROGMEM DEVEUI[8]  = {0x04, 0xF0, 0x04, 0xD0, 0x7E, 0xD5, 0xB3, 0x70  };

    // This key should be in big endian format (or, since it is not really a
    // number but a block of memory, endianness does not really apply). In
    // practice, a key taken from ttnctl can be copied as-is.
    // The key shown here is the semtech default key.
    static const u1_t PROGMEM APPKEY[16] = { 0x51, 0xF4, 0x20, 0x4F, 0x2D, 0xE8, 0xDF, 0x0C, 0xF9, 0xE8, 0xCA, 0xD3, 0x5F, 0xA5, 0xCB, 0x28 };

#endif
